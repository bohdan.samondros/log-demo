package org.home;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggerDemo {
    private static final Logger log = LoggerFactory.getLogger(LoggerDemo.class);

    public static void main(String[] args) {
        log.error("Process has started");
        System.out.println("Hello");
    }
}
