package org.home;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogExceptionDemo {
    private static final Logger log = LoggerFactory.getLogger(LogExceptionDemo.class);

    public static void main(String[] args) {
        try {
            if (true) {
                throw new RuntimeException("Exception with message");
            }
        } catch (Exception ex) {
            log.error(Markers.EXCEPTION, "something went wrong", ex);
        }
    }
}
