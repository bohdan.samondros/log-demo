package org.home;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

public class Markers {
    public static final Marker EXCEPTION = MarkerFactory.getMarker("EXCEPTION");
}
